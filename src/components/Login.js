import React,{ Component } from "react";
import {TextField,Button,Typography} from "@material-ui/core"
class Login extends Component {
    render() {
        return (
            <div>
                <form action="http://localhost:3000/login" method="POST">
                < Typography variant = "title" gutterBottom >Login </Typography>
					< TextField required id = "email" label = "Email" margin = "normal"  type="email"onChange={this.props.change}/><br/>
					< TextField required id = "password" label = "Password" margin = "normal"  type="password" onChange={this.props.change}/><br/><br/>
					< Button variant = "contained" color = "primary" type = "submit"  > Login </Button><br/>

                </form> 
            </div>
        )
    }
}

export default Login;