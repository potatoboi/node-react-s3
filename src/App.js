import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Login from './components/Login';

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email:"",
      password:""
    }
    this.change=this.change.bind(this)
  }
  change = (e) => {
    // console.log(e.target.value)
    this.setState({
      [e.target.id]:e.target.value
    })
  }
  render() {
    return (
      <div className="App">
      <Login change={this.change}/>
		
		<h4>Email:{this.state.email}</h4>
		<h4>Password:{this.state.password}</h4>
		
		
      </div>
    );
  }
}

export default App;
